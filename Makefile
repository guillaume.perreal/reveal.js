export SHELL := /bin/bash

.PHONY: all clean dist

# commentaire

all: dist

clean:
	npx rimraf build/ dist/ node_modules/

dist: build/js/reveal.min.js build/css/theme/irstea.css | dist/
	cp -a build/{js,lib,plugin} dist/
	mkdir -p dist/css/theme
	cp -a build/css/{print,*.css} dist/css
	cp -a build/css/theme/irstea.css dist/css/theme

build/js/reveal.min.js: build/node_modules/
	cd build; node_modules/.bin/grunt jshint uglify

build/css/theme/irstea.css: build/css/theme/source/irstea.scss build/node_modules/
	cd build; node_modules/.bin/grunt css

build/css/theme/source/irstea.scss: build/
	cp -a src/* build/

build/node_modules/: build/package.json
	cd build; npm install

build/gruntfile.js build/package.json: node_modules/ | build/
	cp -a node_modules/reveal.js/{css,js,lib,plugin,package.json,gruntfile.js} build/

node_modules/: package.json
	npm install

build/ dist/:
	mkdir -p $@
